# PJI-CNN-LMP

Nous souhaitons, grâce à ce projet, voir comment les mouvements d'un visage, à travers le descripteur LMP, peuvent être utilisés au sein d’un processus d’apprentissage profond dans le domaine de la reconnaissance faciale. Cet apprentissage se fera à l’aide des réseaux de neurones convolutionnels. 


##  Dossier : dataset 
Exemple d'appel:
```python
trainset = ds.gestionDataset('./data/train'+str(x)+'.txt')
```
Description :

    Le fichier dataset permet à partir d'un fichier de créer un dataset adapté au réseau de neuronne.

    - dataset_1D  : renvoie une ligne de données 1 dimension
    - dataset_2D  : renvoie une matrice de données
    - dataset_Lineair : renvoie une ligne de données

## Dossier : model 
Exemple d'appel:

```python
model = md.Net()
```

Description :

    Le fichier model permet de représenter un modèle de réseau de neuronne.

    - M_L2 : 2 couches linéaires
    - M1D_L2_C1 : 2 couches linéaires et 1 couches convolutionnelles 1D.
    - M1D_L2_C2 : 2 couches linéaires et 2 couches convolutionnelles 1D.
    - M2D_L2_C1 : 2 couches linéaires et 1 couches convolutionnelles 2D.
    - M2D_L2_C2 : 2 couches linéaires et 2 couches convolutionnelles 2D.

## Dossier : model_combined

    Contient les mêmes modeles que model mais avec une jointure des données, est associée au main_combined

## Dossier : data

Description:

    Contient nos données brutes et lors de l'execution du programme vont apparaitre les fichiers de trains et tests des différentes configurations de test. 

Exemple de fichier :

    "CK327.txt"

## Fichier : outils
    Le fichier outils contient différentes fonctions dites outils. 
    - accurancy : calcul la performance du réseau

```python
accurency(donneetest,model)
```
    - cut10toTrainValidtest : crée 10 folds équilibrés de données à partir d'un fichier de données brutes.

```python
cut10ToTrainValidTest(chemin_data+"/"+fichier_data)
```
    - fusionneFile : fusionne une liste de fichier en un seul fichier
```python
fusionneFile(9,listeTrain,str(x))
```
    - inverseData : inverse les données rangées en bin -> histo en histo -> bin
    
```python
data = inverseData(data,nb_batch)
```
## Fichier : extends
    Le fichier extends permet d'aggrandir nos données brutes en ajoutant du bruit.

```python
extend.create_data("data/CK327.txt",10,"data/new_CK327.txt")
```   

## Fichier : main_inversed

    Contient un main permettant de lancer les tests histo -> bin au lieu de bin->histo

## Fichier : main_filtre

    Contient un main permettant de lancer les tests du nombre de filtre dans les convolutions

## Fichier : main_ancienne_version

    Contient la première version du main avec 100 tests et un cross validation

## Fichier : main_combined

    Contient un main permettant de lancer les tests avec des données combinées

## Dossier : image_rapport 

    Contient des schémas pour le rendu du projet

# Compte rendu du projet 

https://www.overleaf.com/read/bpqgfdshyztz