# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
	def __init__(self,nf1,nf2):
		super(Net, self).__init__()
		self.conv1 = nn.Conv1d(1, nf1, kernel_size=3, padding=1, stride=1)
		self.conv2 = nn.Conv1d(nf1, nf1+nf2, kernel_size=3, padding=1, stride=1)
		self.fc1 = nn.Linear(int(300/2)*(nf1+nf2) , 220)
		self.fc2 = nn.Linear(220, 7)
	

		
	def forward(self, x):
		x = self.conv1(x)
		x = self.conv2(x)
		x = F.max_pool1d(x,kernel_size=2, stride=2)
		x = x.view(x.size(0), -1)
		x = F.relu(self.fc1(x))
		x = self.fc2(x)
		return x


#test
