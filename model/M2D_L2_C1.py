# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
	def __init__(self,nombre_filtre):
		super(Net, self).__init__()
		self.conv1 = nn.Conv2d(1, nombre_filtre, kernel_size=3, padding=1, stride=1)
		self.fc1 = nn.Linear(12*6*nombre_filtre, 220)
		self.fc2 = nn.Linear(220, 7)

		
	def forward(self, x):
		x = self.conv1(x)
		x = F.max_pool2d(x,kernel_size=2, stride=2)
		x = x.view(x.size(0), -1)
		x = F.relu(self.fc1(x))
		x = self.fc2(x)
		return x


#test
