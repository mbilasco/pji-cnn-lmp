# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()
		self.fc1 = nn.Linear(25 * 12 , 256)
		self.fc2 = nn.Linear(256, 7)
		
	def forward(self, x):
		x = F.relu(self.fc1(x))
		x = self.fc2(x)
		return x



#test
