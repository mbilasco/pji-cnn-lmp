# -*- coding: utf-8 -*-
import numpy as np
from pathlib import Path
import os
import torch
import outils as ot


class outils():

	#Fonction qui permet de calculer l'acurrency avec un model combiné :
	# - plus le % est grand plus notre réseau est bon
	def accurencyCombined(self,donnee_test,model,mod):
		correct = 0
		total = 0
		with torch.no_grad():
			for data in donnee_test:
				labels = torch.from_numpy(np.array(data[0]-1))
				inputs = torch.from_numpy(np.array( data[1], dtype=np.single))
				invputs = ot.outils().inversedata(inputs,donnee_test.batch_size,mod)
				outputs = model(inputs,invputs)
				_, predicted = torch.max(outputs.data, 1)
				total += labels.size(0)
				#print("p" ,predicted)
				#print("l" ,labels)
				correct += (predicted == labels).sum().item()
		
		return (100 * correct / total)
	
	#Fonction qui permet de calculer l'acurrency :
	# - plus le % est grand plus notre réseau est bon
	def accurency(self,donnee_test,model):
		correct = 0
		total = 0
		with torch.no_grad():
			for data in donnee_test:
				labels = torch.from_numpy(np.array(data[0]-1))
				inputs = torch.from_numpy(np.array( data[1], dtype=np.single))
				outputs = model(inputs)
				_, predicted = torch.max(outputs.data, 1)
				total += labels.size(0)
				#print("p" ,predicted)
				#print("l" ,labels)
				correct += (predicted == labels).sum().item()
		
		return (100 * correct / total)

		#Fonction qui permet de calculer l'acurrency avec des données inversées :
	# - plus le % est grand plus notre réseau est bon
	def accurencyInversed(self,donnee_test,model,mod):
		correct = 0
		total = 0
		with torch.no_grad():
			for data in donnee_test:
				labels = torch.from_numpy(np.array(data[0]-1))
				inputs = torch.from_numpy(np.array( data[1], dtype=np.single))
				invputs = ot.outils().inversedata(inputs,len(inputs),mod)
				outputs = model(invputs)
				_, predicted = torch.max(outputs.data, 1)
				total += labels.size(0)
				#print("p" ,predicted)
				#print("l" ,labels)
				correct += (predicted == labels).sum().item()
		
		return (100 * correct / total)

	
	#Fonction qui permet de couper un fichier en 10 échantillons de distribution équivalente:
	# - 8 pour le train
	# - 1 pour la validation
	# - 1 pour le test 
	def cut10ToTrainValidTest(self,filename):
		
		classes = []
		y_file= open(filename, "r")
		lines = y_file.readlines()
		y_file.close()
		np.random.shuffle(lines)
		
		#récupere toutes les classes
		for line in lines :
			datas = line.split(" ",1)
			classes.append(datas[0])
		
		classes = np.unique(classes)
		#range les classes dans un grand tableau
		all = []
		for i in range (0,len(classes)):
			all.append([])
			
		for line in lines :
			datas = line.split(" ",1)
			all[int(datas[0])-1].append(line)
		
		train_lines1 = ""
		train_lines2 = ""
		train_lines3 = ""
		train_lines4 = ""
		train_lines5 = ""
		train_lines6 = ""
		train_lines7 = ""
		train_lines8 = ""
		train_lines9 = ""
		train_lines10 = ""
		n_split = 10
		for i in range(0,len(classes)):
			ind = len(all[i])/n_split
			ind = np.around(ind)
			ind = int(ind)
			surplus = len(all[i])-(ind*10)
			
			#print(surplus)
			for j in range(0,ind):
				train_lines1 = train_lines1 + all[i][j]
			for j in range(ind,ind*2):
				train_lines2 = train_lines2 + all[i][j]
			for j in range(ind*2,ind*3):
				train_lines3 = train_lines3 + all[i][j]
			for j in range(ind*3,ind*4):
				train_lines4 = train_lines4 + all[i][j]
			for j in range(ind*4,ind*5):
				train_lines5 = train_lines5 + all[i][j]
			for j in range(ind*5,ind*6):
				train_lines6 = train_lines6 + all[i][j]
			for j in range(ind*6,ind*7):
				train_lines7 = train_lines7 + all[i][j]
			for j in range(ind*7,ind*8):
				train_lines8 = train_lines8 + all[i][j]	
			for k in range(ind*8,ind*9):
				train_lines9 = train_lines9 + all[i][k]
			for f in range(ind*9,ind*10+surplus):
				train_lines10 = train_lines10 + all[i][f]
				
		train_lines=[]
		train_lines.append(train_lines1)
		train_lines.append(train_lines2)
		train_lines.append(train_lines3)
		train_lines.append(train_lines4)
		train_lines.append(train_lines5)
		train_lines.append(train_lines6)
		train_lines.append(train_lines7)
		train_lines.append(train_lines8)
		train_lines.append(train_lines9)
		train_lines.append(train_lines10)
		
		for i in range(0,n_split):
			
			file_fold = Path("./data/fold"+str(i)+".txt")
			
			if os.path.isfile(file_fold):
					os.remove(file_fold)	
					
			fold_file = open(file_fold,"x")
			
			for line in train_lines[i] :
				fold_file.write(line)

			fold_file.close()
			
	# Fonction qui fusionne n fichier dans un fichier
	# - n_file = nombre de fichier
	# - arrayFile = la liste des paths de fichier
	# - x = numero de configuration
	def fusionneFile(self,n_file,arrayFile,x):
		data = ""
		for file in arrayFile:
			with open(file) as fp :
				data = data + fp.read()
				
			file_train = "./data/train"+x+".txt" 
			
			if os.path.isfile(file_train):
					os.remove(file_train)	
		
			
			with open(file_train,'x') as fp:
				fp.write(data)
				
			fp.close()
			

	#passe de bin -> histo à histo -> bin
	def inversedata(self,data,nb_batch,model):
		if model == "1d":
			transformeddata = data
			for k in range(0,nb_batch):
				databis = data[k].numpy()[0]
				
				#------------INVERSION HIST ET BIN------------------#
				newdata = list()
				for i in range(0,12):
					for j in range(0,len(databis),12):
						newdata.append(databis[j+i])
				#------------INVERSION HIST ET BIN------------------#
				
				transformeddata[k] = torch.from_numpy(np.asarray(newdata))
		if model == "2d":
			
			transformeddata = torch.from_numpy(np.transpose(data.numpy(),(0, 1, 3, 2)))

			for k in range(0,nb_batch):
				databis = data[k].numpy()
				
				transformeddata[k]=torch.from_numpy(np.transpose(databis, (0, 2, 1)))
				

		if model == "lin":
			transformeddata = data
			for k in range(0,nb_batch):
				databis = data[k].numpy()
				newdata = list()
				for i in range(0,12):
					for j in range(0,len(databis),12):
						newdata.append(databis[j+i])
				#------------INVERSION HIST ET BIN------------------#
				
				transformeddata[k] = torch.from_numpy(np.asarray(newdata))

		return transformeddata
		
