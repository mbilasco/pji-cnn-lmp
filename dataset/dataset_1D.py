# -*- coding: utf-8 -*-
from torch.utils.data import Dataset
import torch
import numpy as np
class gestionDataset(Dataset):

	def __init__ (self,filename):
		self.nbins = 12
		self.conteneur = []
		file = open(filename)

		
		for i, line in enumerate(file):
			#Prends les datas d'une expression
			line = line.split(None, 1)
			
			#On enleve les espaces
			lineC = line[1].split(' ')
			
			#On enleve le EOF
			lineC = lineC[:-1]
			
			#Conversion en nombre
			data = []
			
			data = list(map(float, lineC))

			dataLine = []
			
			#numero d'expression
			exp = int(line[0])
			

			#ajout du num d'expression + data de l'exp
			dataLine.append(exp)
			dataLine.append(data)
			
			#ajout au conteneur globale
			self.conteneur.append(dataLine)
			
		file.close()
		
		
	def __getitem__(self,key):
		x=np.expand_dims(np.asarray(self.conteneur[key][1]), axis=0)
		return [self.conteneur[key][0],torch.tensor(x)]
	
	def __len__(self):
		return len(self.conteneur)




