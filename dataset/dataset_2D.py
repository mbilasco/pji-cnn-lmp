# -*- coding: utf-8 -*-
from torch.utils.data import Dataset
import torch
import numpy as np
class gestionDataset(Dataset):

	def __init__ (self,filename):
		self.nbins = 12
		self.conteneur = []
		file = open(filename)

		
		for i, line in enumerate(file):
			#Prends les datas d'une expression
			line = line.split(None, 1)
			
			#On enleve les espaces
			lineC = line[1].split(' ')
			
			#On enleve le EOF
			lineC = lineC[:-1]
			
			#Conversion en nombre
			data = []
			
			for i in range(0,25):
				linedata = []
				for j in range(0,12):
					linedata.append(float(lineC[i*j]))
				data.append(linedata)
	
			dataLine = []
			
			#numero d'expression
			exp = int(line[0])			
			
			#ajout du nexp + data de l'exp
			dataLine.append(exp)
			dataLine.append(data)
			
			#ajout au conteneur globale
			self.conteneur.append(dataLine)
			
		file.close()
		
		
	def __getitem__(self,key):
		x=np.expand_dims(np.asarray(self.conteneur[key][1]), axis=0)
		return [self.conteneur[key][0],torch.tensor(x)]
	
	def __len__(self):
		return len(self.conteneur)


