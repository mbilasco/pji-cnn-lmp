# -*- coding: utf-8 -*-
import random as rd
import os

class create_data:
    
    def __init__ (self,filename,facteur,new_file_name):
        """
        Creation de données a partir d'un fichier de base
        facteur (int) : nb de duplicata d'une ligne avec modification
        new_file_name (str) : nom du fichier pour les données dupliquées
        """
        self.new_file_name = new_file_name

        if(facteur > 1 and type(facteur) == int):
            self.factor = facteur
        else:
            raise ValueError('Le coefficient de multiplication des données est incorrect. Il doit être entier et supérieur à 1.')       
    
        self.extract_data(filename)
        
    def extract_data(self,filename):
        file = open(filename)
        if os.path.isfile(self.new_file_name):
                os.remove(self.new_file_name)
        for i, line in enumerate(file):
            #Prends les datas d'une expression
            line = line.split(None, 1)
            
            #On enleve les espaces
            lineC = line[1].split(' ')
            
            #On enleve le EOF
            lineC = lineC[:-1]
            
            #Conversion en nombre
            data = list(map(float, lineC))
            
            dataLine = []
            
            #numero d'expression
            exp = int(line[0])
            
            #ajout du n° d'exp + data de l'exp
            dataLine.append(exp)
            dataLine.append(data)
            
            self.modify_data(dataLine)
        
        file.close()


    def modify_data(self,dataLine):
        """
        Duplication de la line factor fois avec modification d'une valeur a chaque fois
        """
        #duplication de la ligne factor fois avec une modification d'une valeur
        
        	
                
        for j in range(self.factor):

            #Récupére d'un indice pour l'un des bins
            ind = rd.randint(0,len(dataLine[1])-1)

            #Valeur a l'indice ind
            some_bin = dataLine[1][ind]

            #Recupération de 10%
            some_bin_10 = some_bin * 0.1

            #Ajout ou retrait de 10% à la valeur en fonction d'un nombre random
            if(rd.random() > 0.5):
                some_bin += some_bin_10
            else:
                some_bin -= some_bin_10

            #Modification de la valeur à l'indice ind par sa nouvelle valeur
            dataLine[1][ind] = some_bin

            #Ecriture dans le fichier de la nouvelle ligne
            self.write_data(dataLine)


    def write_data(self,dataLine):
        """
        Ecrit une ligne dans le fichier avec les données d'une dataLine
        """
        #Creation de la ligne à ecrire dans le fichier
        line = ""
        line += str(dataLine[0]) + " "
        
        for v in dataLine[1]:
                     line += str(int(v))
                     line += " "

        #Suppression du dernier espace inutile et ajout d'un retour a la ligne
       # line = line[:-1]
        line += "\n"

        #Ouverture du fichier dans lequel dupliquer les données, écriture et fermeture
        new_file = open(self.new_file_name,"a")
        new_file.write(line)
        new_file.close()

if __name__ == "__main__":

   c = create_data("data/CK327.txt",10,"data/test_new_data.txt")

   
   
