# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F
import torch

class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()
		self.conv1 = nn.Conv1d(1, 2, kernel_size=3, padding=1, stride=1)
		self.conv2 = nn.Conv1d(2, 4, kernel_size=3, padding=1, stride=1)
		self.fc1 = nn.Linear(int(300/2)*4 , 220)
		self.fc2 = nn.Linear(440, 7)
	

		
	def forward(self, x1,x2):
		xa = self.conv1(x1)
		xa = self.conv2(xa)
		xb = self.conv1(x2)
		xb = self.conv2(xb)

		xa = F.max_pool1d(xa,kernel_size=2, stride=2)
		xa = xa.view(xa.size(0), -1)
		xb = F.max_pool1d(xb,kernel_size=2, stride=2)
		xb = xb.view(xb.size(0), -1)

		xa = F.relu(self.fc1(xa))
		xb = F.relu(self.fc1(xb))

		combined = torch.cat((xa.view(xa.size(0), -1),
                        xb.view(xb.size(0), -1)), dim=1)
		
		x = self.fc2(combined)
		return x


#test
