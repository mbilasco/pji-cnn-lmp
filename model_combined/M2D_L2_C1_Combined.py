# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F
import torch
class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()
		self.conv1 = nn.Conv2d(1, 4, kernel_size=3, padding=1, stride=1)
		self.fc1 = nn.Linear(288 , 220)
		self.fc2 = nn.Linear(440, 7)
	

		


	def forward(self, x1,x2):
		xa = self.conv1(x1)
		xb = self.conv1(x2)

		xa = F.max_pool2d(xa,kernel_size=2, stride=2)
		xa = xa.view(xa.size(0), -1)
		xb = F.max_pool2d(xb,kernel_size=2, stride=2)
		xb = xb.view(xb.size(0), -1)

		xa = F.relu(self.fc1(xa))
		xb = F.relu(self.fc1(xb))

		combined = torch.cat((xa.view(xa.size(0), -1),
                        xb.view(xb.size(0), -1)), dim=1)
		
		x = self.fc2(combined)
		return x


#test
